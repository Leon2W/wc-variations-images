<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>
<li <?php wc_product_class(); ?>>
	<?php
	/**
	 * Hook: woocommerce_before_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_open - 10
	 */
	do_action( 'woocommerce_before_shop_loop_item' );

	/**
	 * Hook: woocommerce_before_shop_loop_item_title.
	 *
	 * @hooked woocommerce_show_product_loop_sale_flash - 10
	 * @hooked woocommerce_template_loop_product_thumbnail - 10
	 */
	do_action( 'woocommerce_before_shop_loop_item_title' );
	
	
	
	
	
	// Everything above here is standard.
	//
	// Lets add our custom wc-loop image here
	// check the query which color we're asking for 
	global $wp_query;
	foreach($wp_query->query_vars['tax_query'] as $values){
		if(isset($values['taxonomy'])){
			if($values['taxonomy'] == 'pa_kleur'){
				$color = $values['terms'][0];
			}
		}
	}
	// Now that we know the color, get all the variations
	$variations = $product->get_available_variations();
	foreach($variations as $key => $val){
		// check if kleur attribute is set
		if(isset($val['attributes']['attribute_pa_kleur'])){
			if($val['attributes']['attribute_pa_kleur'] == $color){
				$var = $key;
			}
		}
	}
	// check if we have results first
	if(!empty($color) && isset($var)){
		// if so, use the variation image
		$variation = $variations[$var];
		$img = $variation['image']['thumb_src'];
		if(empty($img)){
			// if it was somehow still empty, fall back to default thumbnail
			$img = get_the_post_thumbnail_url();
		}
	} else {
		// Else, show the default thumbnail
		$img = get_the_post_thumbnail_url();
	}
	// echo the image
	echo '<img src="'.$img.'" />';
	//
	//
	//
	// disclaimer: Quick & dirty solution. Not tested for performance and compatibility.
	//
	// Everything below here is standard.
	
	
	
	
	
	
	

	/**
	 * Hook: woocommerce_shop_loop_item_title.
	 *
	 * @hooked woocommerce_template_loop_product_title - 10
	 */
	do_action( 'woocommerce_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_after_shop_loop_item_title.
	 *
	 * @hooked woocommerce_template_loop_rating - 5
	 * @hooked woocommerce_template_loop_price - 10
	 */
	do_action( 'woocommerce_after_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_after_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_close - 5
	 * @hooked woocommerce_template_loop_add_to_cart - 10
	 */
	do_action( 'woocommerce_after_shop_loop_item' );
	?>
</li>